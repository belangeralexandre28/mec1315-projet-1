def agrandissementFVN(f,v,n,x,y,z):
    from MEC1315_STL import ReadSTL,CalculNormal
    #l'objet est un nom de fichier qui se trouve dans le même dossier
    #retourne un objet agrandit selon les axes x y et z
    
    #f,v,n=ReadSTL(objet)
    
    v[:,0],v[:,1],v[:,2]=v[:,0]*x,v[:,1]*y,v[:,2]*z
    
    n=CalculNormal(f,v)
    
    return f,v,n

def agrandissementOBJET(objet,x,y,z):
    
    from MEC1315_STL import ReadSTL,CalculNormal,EcrireSTLASCII
    #l'objet est un nom de fichier qui se trouve dans le même dossier
    #retourne un objet agrandit selon les axes x y et z
    
    f,v,n=ReadSTL(objet)
    
    v[:,0],v[:,1],v[:,2]=v[:,0]*x,v[:,1]*y,v[:,2]*z
    
    n=CalculNormal(f,v)
    nomout=f'{ojtet}_agrandit{x},{y},{z}'
    EcrireSTLASCII(nomout)
    
    return nomout
    